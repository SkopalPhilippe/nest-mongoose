import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { RobotsModule } from './robots/robots.module';
import * as data from './secret.json'

@Module({
  imports: [RobotsModule,
  // MongooseModule.forRoot('mongodb://thesmarthyena:Rejvesh789%3F@mongodb-thesmarthyena.alwaysdata.net:27017/thesmarthyena_mongo', {useNewUrlParser: true, useUnifiedTopology: true}),
  MongooseModule.forRoot(data.secret, {useNewUrlParser: true, useUnifiedTopology: true}),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
