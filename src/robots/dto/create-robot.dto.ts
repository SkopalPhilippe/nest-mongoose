import { IsString } from "class-validator"

export class CreateRobotDto {
    @IsString()
    readonly name: string;
}
