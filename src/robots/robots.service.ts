import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateRobotDto } from './dto/create-robot.dto';
import { UpdateRobotDto } from './dto/update-robot.dto';
import { Model } from 'mongoose';
import { Robot } from './entities/robot.entity';

@Injectable()
export class RobotsService {

  constructor(
    @InjectModel(Robot.name) private readonly robotModel: Model<Robot>
  ) {}

  create(createRobotDto: CreateRobotDto) {
    const robot = new this.robotModel(createRobotDto);
    return robot.save();
  }

  async findAll() {
    return await this.robotModel.find().exec();
  }

  findOne(id: string) {
    return this.robotModel.findById({_id: id}).exec();
  }

  async update(id: string, updateRobotDto: UpdateRobotDto) {
    const existingRobot = await this.robotModel
    .findOneAndUpdate({_id: id}, {$set: updateRobotDto}, {new: true})
    .exec();    

    if (!existingRobot) {
      throw new NotFoundException(`Robot #${id} not found`);
    }
    return existingRobot;
  }

  async remove(id: string) {
    const robot = await this.robotModel.findOne({_id: id});
    return robot.remove();
  }
}
