import { Module } from '@nestjs/common';
import { RobotsService } from './robots.service';
import { RobotsController } from './robots.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Robot, RobotSchema } from './entities/robot.entity';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: Robot.name,
        schema: RobotSchema
      }
    ])
  ],
  controllers: [RobotsController],
  providers: [RobotsService]
})
export class RobotsModule {}
